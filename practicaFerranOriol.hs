-------------------------------------------------------------------------------------
----------------------------PRÀCTICA DE HASKELL 2018---------------------------------
-------------------------------------------------------------------------------------
-------------------------Ferran Rodríguez, Oriol Hurtós ------------------------------
import Data.List
import Data.Maybe
import Data.Char

------------------------------ Definició de CONSTANTS --------------------------------
--------------------------------------------------------------------------------------

taulerInicial = Tauler [[(TN,('a',8)),(CN,('b',8)),(AN,('c',8)),(DN,('d',8)),(RN,('e',8)),(AN,('f',8)),(CN,('g',8)),(TN,('h',8))],
                        [(PN,('a',7)),(PN,('b',7)),(PN,('c',7)),(PN,('d',7)),(PN,('e',7)),(PN,('f',7)),(PN,('g',7)),(PN,('h',7))],
						[(B,('a',6)),(B,('b',6)),(B,('c',6)),(B,('d',6)),(B,('e',6)),(PN,('f',6)),(B,('g',6)),(B,('h',6))],
						[(B,('a',5)),(B,('b',5)),(B,('c',5)),(B,('d',5)),(B,('e',5)),(B,('f',5)),(B,('g',5)),(B,('h',5))],
						[(B,('a',4)),(B,('b',4)),(B,('c',4)),(B,('d',4)),(B,('e',4)),(B,('f',4)),(B,('g',4)),(B,('h',4))],
						[(B,('a',3)),(B,('b',3)),(B,('c',3)),(B,('d',3)),(B,('e',3)),(B,('f',3)),(B,('g',3)),(B,('h',3))],
						[(PB,('a',2)),(PB,('b',2)),(PB,('c',2)),(PB,('d',2)),(PB,('e',2)),(PB,('f',2)),(PB,('g',2)),(PB,('h',2))],
						[(TB,('a',1)),(CB,('b',1)),(AB,('c',1)),(DB,('d',1)),(RB,('e',1)),(AB,('f',1)),(CB,('g',1)),(TB,('h',1))]]


---------------------------- Definició de TIPUS DE DADES -----------------------------
--------------------------------------------------------------------------------------

type Posicio = (Char, Int)
type Partida = (Tauler, Color)
type Jugada = (Peca, Posicio,Posicio)

data Peca = PN | PB | CN | CB | AN | AB | TN | TB | DN | DB | RN | RB | B deriving (Eq)
data Tauler = Tauler [[(Peca, Posicio)]]
data Color = Blanc | Negre deriving (Eq,Show)
-- És el color del jugador


----------------------------------- INSTANCIACIONS ------------------------------------
--------------------------------------------------------------------------------------

instance Show Peca where
 show PN = "p"
 show PB = "P"
 show CN = "c"
 show CB = "C"
 show AN = "a"
 show AB = "A"
 show TN = "t"
 show TB = "T"
 show DN = "d"
 show DB = "D"
 show RN = "r"
 show RB = "R"
 show B = "."
 
instance Show Tauler where
  show (Tauler t) = "   _________ \n" ++ "8 |"++mostrarFila (t!!0)++"|\n" ++ "7 |"++mostrarFila (t!!1)++"|\n" ++ "6 |"++mostrarFila (t!!2)++"|\n" 
				++ "5 |"++mostrarFila (t!!3)++"|\n" ++ "4 |"++mostrarFila (t!!4)++"|\n" ++ "3 |"++mostrarFila (t!!5)++"|\n" ++ "2 |"++mostrarFila (t!!6)++"|\n"
				++ "1 |"++mostrarFila (t!!7)++"|\n" ++ "   _________ \n   abcdefgh"

-------------------------------------- PROGRAMA --------------------------------------
--------------------------------------------------------------------------------------
main = do
  putStrLn "Introdueix el nom del fitxer de la partida que vos carregar:"
  name <- getLine
  putStrLn ("Carregant... " ++ name ++ ", espera un moment si us plau")
  {-k <- readFile name -- AIXO llefeix el txt
  putStrLn ("Fitxer... \n" ++ k ++ " .")
  print(dividirTauler (mostrarTauler taulerInicial)) 	-- AMB AIXO MOSTRES EL RESULTAT D'UNA FUNCIO PER PANTALLA -}
  

  
-------------------------------------- FUNCIONS --------------------------------------
--------------------------------------------------------------------------------------
   
-- Mostra totes les peces que hi ha a una array donada
mostrarFila :: [(Peca, Posicio)] -> String
mostrarFila [] = error "No hi ha cap peça"
mostrarFila [x] = show (fst(x))
mostrarFila (x:xs) = show (fst(x)) ++ mostrarFila xs


{- La funció dinsDelsMarges retorna cert si la posició es dins dels marges, altrament fals
   No se com fer de moment per comprovar que la Lletra pertany als marges del tauler -}
dinsDelsMarges :: Posicio -> Bool
dinsDelsMarges (x,y)
  | y<1 || y>8 = False
  | x>'h' || x<'a' = False
  | otherwise = True

{- Retorna tots els moviments del rei possible a través de la Posicio entrada.
   Un Rei pot fer un salt en qualsevol direcció, sense sortir-se del tauler -}
movimentRei :: Posicio -> [Posicio]
movimentRei (g,h) = (vt (g,h) 1) ++ (vt (g,h) (-1)) ++ (vt (g!+1,h) 0) ++ (vt (g!+(-1),h) 0) ++ (vt (g!+1,h) 1) ++ (vt (g!+1,h) (-1)) ++ (vt (g!+(-1),h) (-1)) ++ (vt (g!+(-1),h) (1))
  where 
      vt (g,h) x = if (dinsDelsMarges (g,h+x)) then [(g,h+x)] else []
	  
{- Retorna tots els moviments del peo possible a través de la Posicio entrada.
   Un Peó pot fer 1 o 2 salts en la primera jugada i només 1 salt en la resta.
   Com que no es sap qui és el jugador que està efectuant el moviment es considerara ...  -}
movimentPeo :: Posicio -> [Posicio]
movimentPeo (g,h) = if (h==2) then ((vt (g,h) 1) ++ (vt (g,h) 2)) else (vt (g,h) 1)
  where 
      vt (g,h) x = if (dinsDelsMarges (g,h+x)) then [(g,h+x)] else []
	  
--- COMPROVAT FINS AQUÍ

{- Retorna tots els moviments del Cavall possible a través de la Posicio entrada.
   Un Cavall fa salts en forma de L i pot tenir fins a un total de 8 possibilitats -}
movimentCavall :: Posicio -> [Posicio]
movimentCavall (g,h) = (vld (g,h+1) (-2)) ++ (vld (g,h+1) (2)) ++ (vld (g,h-1) (-2)) ++ (vld (g,h-1) (2)) ++ (vld (g,h+2)(-1)) ++ (vld (g,h+2) (1)) ++ (vld (g,h-2) (-1)) ++ (vld (g,h-2) (1))
  where
     vld (g,h) a = if (dinsDelsMarges (g!+a,h)) then [(g!+a,h)] else []
	 
{- Retorna tots els moviments de l'alfil possible a través de la Posicio entrada.
   Un Alfil fa salts en diagonal i pot tenir diferent número de possibilitats depenent de la posició on es trobi -}
movimentAlfil :: Posicio -> [Posicio]
movimentAlfil (g,h) = (diagEsAd (g,h+1) (-1)) ++ (diagEsAb (g,h-1) (-1)) ++ (diagDrAd (g,h+1) (1)) ++ (diagDrAb (g,h-1) (1))

{- Retorna tots els moviments de la torre possible a través de la Posicio entrada.
   Una torre fa salts en horitzontal i pot tenir diferent número de possibilitats depenent de la posició on es trobi -}
movimentTorre :: Posicio -> [Posicio]
movimentTorre (g,h) = (hrz (g,h) (1)) ++ (hrz (g,h) (-1)) ++ (vrt (g,h) 1) ++ (vrt (g,h) (-1))

{- La funció moviment ... saber donada una Peca i una posició, quines serien totes les posicions on podria anar en un tauler buit. -}
moviment :: Peca -> Posicio -> [Posicio]
moviment x y
  | (x == PN) || (x == PB) = movimentPeo y
  | (x == CN) || (x == CB) = movimentCavall y
  | (x == AN) || (x == AB) = movimentAlfil y
  | (x == TN) || (x == TB) = movimentTorre y
  | (x == DN) || (x == DB) = (movimentTorre y)++(movimentAlfil y)
  | (x == RN) || (x == RB) = movimentRei y
  | otherwise = error "No existeix aquesta Peca"

{- La funció alguEntre retorna cert si hi ha algú entre les posicions donades, sense contemplar aquestes dues. -}
alguEntre :: Tauler -> Posicio -> Posicio -> Bool
alguEntre t (a,b) (x,y)
  | a == x && b == y = False
  | a == x && b < y = alguPosicions t (init (tail (vertical (x,y) (a,b))))
  | a == x && b > y = alguPosicions t (init (tail (vertical (a,b) (x,y))))
  | a > x && b == y = alguPosicions t (init (tail (horitzontal (a,b) (x,y))))
  | a < x && b == y = alguPosicions t (init (tail (horitzontal (x,y) (a,b))))
  | a < x && b < y = alguPosicions t (init (tail (diagonalBA (a,b) (x,y))))
  | a < x && b > y = alguPosicions t (init (tail (diagonalAB (x,y) (a,b))))
  | a > x && b < y = alguPosicions t (init (tail (diagonalAB (a,b) (x,y))))
  | a > x && b > y = alguPosicions t (init (tail (diagonalBA (x,y) (a,b))))


{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en horitzontal
   Precondicions: Les dues posicions es troben a la mateixa fila i la primera ha de ser més gran o igual que la segona, i han d'estar dins dels marges del tauler -}
horitzontal :: Posicio -> Posicio -> [Posicio]
horitzontal (a,b) (x,y)
 | (a,b) == (x,y) = [(x,y)]
 | otherwise = [(a,b)] ++ (horitzontal (((!+) a (-1)),b) (x,y))
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en vertical
   Precondicions: Les dues posicions es troben a la mateixa columna, la primera ha de ser més gran o igual que la segona, i han d'estar dins dels marges del tauler -}
vertical :: Posicio -> Posicio -> [Posicio]
vertical (a,b) (x,y)
 | (a,b) == (x,y) = [(a,b)]
 | otherwise = [(a,b)] ++ (vertical (a,b-1) (x,y))

{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en la diagonal que comença per adalt a l'esquerra i acaba abaix a la dreta
   Precondicions: Les dues posicions es troben a la mateixa diagonal, la primera ha de ser la que estroba més cap a la dreta en el tauler, i han d'estar dins dels marges del tauler -}
diagonalAB :: Posicio -> Posicio -> [Posicio]
diagonalAB (a,b) (x,y)
 | (a,b) == (x,y) = [(a,b)]
 | otherwise = [(a,b)] ++ (diagonalAB (((!+)a (-1)),b+1) (x,y))
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en la diagonal que comença per abaix a l'esquerra i acaba adalt a la dreta
   Precondicions: Les dues posicions es troben a la mateixa diagonal, la primera ha de ser la que estroba més cap a la l'esquerra en el tauler, i han d'estar dins dels marges del tauler -}
diagonalBA :: Posicio -> Posicio -> [Posicio]
diagonalBA (a,b) (x,y)
 | (a,b) == (x,y) = [(a,b)]
 | otherwise = [(a,b)] ++ (diagonalBA (((!+)a 1),b+1) (x,y))
 
{- Retorna cert si algunes de les posicions donades té alguna Peça diferent de B, és a dir, si no és buida, altrament fals
   Precondició: Les posicions entrades tenen que ser legals (dins del tauler) -}
alguPosicions :: Tauler -> [Posicio] -> Bool
alguPosicions (Tauler t) [y] = if ((posicioBuida (t!!(canvNum (snd y))) (fst y)) == False) then True else False
alguPosicions (Tauler t) (y:ys) = if ((posicioBuida (t!!(canvNum (snd y))) (fst y)) == False) then True else (alguPosicions (Tauler t) ys)
 

-- Le paso [Peca], i la columna a buscar, la fila ya la se de antes, retorna cert si no hi ha cap peca a la posició donada, altrament fals
posicioBuida :: [(Peca, Posicio)] -> Char -> Bool
posicioBuida [x] p = if (fst (snd x) == p) then (if (fst x == B) then True else False) else True
posicioBuida (x:xs) p = if (fst (snd x) == p) then (if (fst x == B) then True else False) else posicioBuida xs p



-- agafo la array de les peces que te aquesta fila, despres obtinc la posicio on es troba la Posicio a aquesta fila
-- i amb aquestes dues faig !! i obtinc el valor de la peca a la posicio n trobada abans
{-aghgh :: Tauler -> Int -> Posicio -> Peca
aghgh (Tauler t) i p = (map fst (t!!i))!!((elemIndex p (map snd (t!!i))))-}

{-posicioJJJ :: Tauler -> Posicio -> Int -> Int
posicioJJJ (Tauler t) p i = if ((elemIndex p (map snd (t!!i))) == Nothing) then 0 else (obtFila ((elemIndex p (map snd (t!!i)))))-}

{-
 La funció fesJugada retorna el Tauler inicial però amb la jugada efectuada. Si la jugada no es pot fer retorna error
-}
fesJugada :: Tauler -> Jugada -> Tauler
fesJugada t (x,y,z) = (canviTauler (canviTauler t y B) z x)
 
caccsa :: Tauler -> Int -> Int
caccsa (Tauler t) i = snd(snd(head (t!!i)))


{-
 La funció escac ...
-}
escac :: Tauler -> Color -> Bool
escac (Tauler t) c = taulerEscac (Tauler t) 0 c (posAct (Tauler t) c 0 'R')
	 
{- Retorna cert si alguna peca contraria al color donat pot matar al rei del contrari, que es troba a la posicio donada, altrament fals -}
taulerEscac :: Tauler -> Int -> Color -> Posicio -> Bool
taulerEscac (Tauler t) 7 c p = if (filaPecaEscac (t!!7) c p) then True else False
taulerEscac (Tauler t) i c p = if (filaPecaEscac (t!!i) c p) then True else (taulerEscac (Tauler t) (i+1) c p)

{- Paso la fila, el color contrario, la posicion donde esta el rey y devuelvo true si alguna pieca rival puede llegar al rey -}
filaPecaEscac :: [(Peca,Posicio)] -> Color -> Posicio -> Bool
filaPecaEscac [x] c p = if (esRival (fst x) c) then (if (faEscac x p) then True else False) else False
filaPecaEscac (x:xs) c p = if (esRival (fst x) c) then (if (faEscac x p) then True else filaPecaEscac xs c p) else filaPecaEscac xs c p

{- Retorna cert si la peca entrada no pertany al color entrat, és a dir, si són rivals -}
esRival :: Peca -> Color -> Bool
esRival p c = if (((p==PN || p==CN || p==AN || p==TN || p==DN || p==RN) && (c == Blanc)) || ((p==PB || p==CB || p==AB || p==TB|| p==DB|| p==RB) && (c == Negre))) then True else False

{- Retorna cert si la peca en la posició entrada té algun moviment que la porti a la segona posicio, altrament retorna fals -}
faEscac :: (Peca,Posicio) -> Posicio -> Bool
faEscac (x,y) p = if ((find (==p) (moviment x y)) == Nothing) then False else True


-- Tiene que devolver LA POSICION
-- Char en mayuscules
posAct :: Tauler -> Color -> Int -> Char -> Posicio
posAct (Tauler t) c i o = if ((elemIndex (colorPeca c o) (map fst (t!!i)))==Nothing) then (posAct (Tauler t) c (i+1) o) else (obtColCh (elemIndex (colorPeca c o) (map fst (t!!i))),(canvNum i))

-- 
colorRei :: Color -> Peca
colorRei Blanc = RB
colorRei Negre = RN

-- Char en Mayuscules
colorPeca :: Color -> Char -> Peca
colorPeca Blanc x = if (x=='P') then PB else (if (x=='C') then CB else (if (x=='A') then AB else (if (x=='T') then TB else (if (x=='D') then DB else RB))))
colorPeca Negre x = if (x=='P') then PN else (if (x=='C') then CN else (if (x=='A') then AN else (if (x=='T') then TN else (if (x=='D') then DN else RN))))


{-posicioPeca :: Tauler -> Int -> Int
posicioPeca (Tauler t) i = if ((elemIndex (colorRei c) (map fst (t!!i)))==Nothing) then 0 else (obtColCh (elemIndex (colorRei c) (map fst (t!!i))),(canvNum i))
-}

{- Paso la fila, el color contrario, la posicion donde esta el rey y devuelvo true si alguna pieca rival puede llegar al rey -}
--filaPecaEscac :: [(Peca,Posicio)] -> Color -> Posicio

-- alguEntre :: Tauler -> Posicio -> Posicio -> Bool
--esRival :: Peca -> Color -> Bool

{- La funció jugadaLegal: Primer mirem si la jugada indicada la pot fer la peça indicada, si no pot retornem directament fals.
Altrament, si pot, mirem si es cavall o no...
Si no és cavall mirem si hi ha algú pel camí marcat entre les dues posicions, si hi ha algú retornem Fals, altrament mirem si la posició final
indicada és buida o hi ha una peça rival, si compleix una de les dues condicions retorna Cert, és a dir, la jugada és valida, altrament fals. -}
-- COMPROVAR QUE EN LA POSICION ESTE DICHA PIEZA
jugadaLegal :: Tauler -> Jugada -> Bool
jugadaLegal (Tauler t) (p, p1, p2) = if (faEscac (p,p1) p2) then (if ((obtPeca (Tauler t) p1)==CB || (obtPeca (Tauler t) p1)==CN) then (posFinal (Tauler t) p1 p2) else (if (alguEntre (Tauler t) p1 p2) then False else (posFinal (Tauler t) p1 p2))) else False
  where 
    posFinal (Tauler t) p1 p2 = if (((obtPeca (Tauler t) p2)==B) || (esRival (obtPeca (Tauler t) p2) (colorPeca2 (obtPeca (Tauler t) p1)))) then True else False
	
{-
jugadaLegal :: Tauler -> Jugada -> Bool
jugadaLegal (Tauler t) (p, p1, p2) = if (faEscac (p,p1) p2) then (if (esCavall (Tauler t) p1) then (posFinal (Tauler t) p1 p2) else (if (alguEntre (Tauler t) p1 p2) then False else (posFinal (Tauler t) p1 p2))) else False
  where 
    posFinal (Tauler t) p1 p2 = if (((obtPeca (Tauler t) p2)==B) || (esRival (obtPeca (Tauler t) p2) (colorPeca2 (obtPeca (Tauler t) p1)))) then True else False
	esCavall (Tauler t) p1 = if ((obtPeca (Tauler t) p1)==CB || (obtPeca (Tauler t) p1)==CN) then True else False
	-}



-- jugadaLegal (Tauler t) (p, p1, p2) = if (hiHaPeca (Tauler t) (moviment p p1)) then False else True

{- Retorna el color de la Peca. Precondició: La peca ha d'existir i no pot ser B -}
colorPeca2 :: Peca -> Color
colorPeca2 x = if (x==PB || x==CB || x==AB || x==TB || x==DB || x==RB) then Blanc else Negre

{- Retorna cert si a alguna de les posicions donades hi ha alguna peca (Diferent de B, que representa el buid) -}
hiHaPeca :: Tauler -> [Posicio] -> Bool
hiHaPeca (Tauler t) [x] = if ((obtPeca (Tauler t) x)==B) then False else True
hiHaPeca (Tauler t) (x:xs) = if ((obtPeca (Tauler t) x)==B) then (hiHaPeca (Tauler t) xs) else True

-- Retorna la peca que hi ha a la posicio donada al tauler donat
obtPeca :: Tauler -> Posicio -> Peca
obtPeca (Tauler t) (x,y) = fst ((t!!(canvNum y))!!(obtCol(x)-1))

aasdsa :: Tauler -> [(Peca, Posicio)]
aasdsa (Tauler t) = (t!! (canvNum 5))

{-
 La funció escacMat ...
-}
-- escacMat :: Color -> Tauler -> Bool?????





------------------------------- FUNCIONS AUXILIARS -----------------------------------
--------------------------------------------------------------------------------------

{- !+ és un operador que retorna la lletra que ve despres de n salts (Int) de la lletra que hem introduit (Char), si no pertany a cap columna retorna 'z' -}
(!+) :: Char -> Int -> Char
ch !+ a = if ((vv a)<'i' && (vv a)>'`') then vv a else 'z'
  where
     vv a = (chr(ord ch + a))
	 

{- Retorna tots els moviments en diagonal cap a l'esquerra i cap adalt a partir de la posició donada i el numero de salts de columna 'a' -}
diagEsAd :: Posicio -> Int -> [Posicio]
diagEsAd (g,h) a = if (dinsDelsMarges (g!+a,h)) then ([(g!+a,h)] ++ (diagEsAd (g!+a,h+1) (-1))) else []

{- Retorna tots els moviments en diagonal cap a l'esquerra i cap abaix a partir de la posició donada i el numero de salts de columna 'a' -}
diagEsAb :: Posicio -> Int -> [Posicio]
diagEsAb (g,h) a = if (dinsDelsMarges (g!+a,h)) then ([(g!+a,h)] ++ (diagEsAb (g!+a,h-1) (-1))) else []

{- Retorna tots els moviments en diagonal cap a la dreta i cap adalt a partir de la posició donada i el numero de salts de columna 'a' -}
diagDrAd :: Posicio -> Int -> [Posicio]
diagDrAd (g,h) a = if (dinsDelsMarges (g!+a,h)) then ([(g!+a,h)] ++ (diagDrAd (g!+a,h+1) (1))) else []

{- Retorna tots els moviments en diagonal cap a la dreta i cap abaix a partir de la posició donada i el numero de salts de columna 'a' -}
diagDrAb :: Posicio -> Int -> [Posicio]
diagDrAb (g,h) a = if (dinsDelsMarges (g!+a,h)) then ([(g!+a,h)] ++ (diagDrAb (g!+a,h-1) (1))) else []

{- Retorna tots els moviments en horitzontal a partir de la posició donada, la direcció depén del valor de la columna 'a' -}
hrz :: Posicio -> Int -> [Posicio]
hrz (g,h) a = if (dinsDelsMarges (g!+a,h)) then ([(g!+a,h)] ++ (hrz (g!+a,h) a)) else []

{- Retorna tots els moviments en vertical a partir de la posició donada, la direcció depén del valor de la fila 'a' -}
vrt :: Posicio -> Int -> [Posicio]
vrt (g,h) a = if (dinsDelsMarges (g,h+a)) then ([(g,h+a)] ++ (vrt (g,h+a) a)) else []


------------------------------------------------------------------------------------------------------------------------------------------ 

movPec :: [(Peca, Posicio)] -> Int -> Peca
movPec x y = fst(x!!y)


-- Tenim la fila, la posicio de la Columna (Int) que volem canviar, i la peça a canviar
canviPeca :: [(Peca, Posicio)] -> Int -> Peca -> [(Peca, Posicio)]
canviPeca x y z = (take (y-1) x)++[(z,snd(x!!y))]++(drop y x)

{- Treu la peça que hi hagi a la posició donada i la canvia per la Peça donada.
   Variables entrada: Tauler a canviar, Posicio on anirà la peça, Peca a canviar -}
canviTauler :: Tauler -> Posicio -> Peca -> Tauler
canviTauler (Tauler t) (c,f) p = (Tauler ((chg t c f p 0)++(chg t c f p 1)++(chg t c f p 2)++(chg t c f p 3)++(chg t c f p 4)++(chg t c f p 5)++(chg t c f p 6)++(chg t c f p 7)))
  where
      chg t c f p i= if (f==snd(snd(head (t!!i)))) then [(canviPeca (t!!i) (obtCol c) p)] else [(t!!i)]
	  
-- Torna el valor de la fila al revés (8=0, 7=1, ... 1=7)
canvNum :: Int -> Int
canvNum x = if ((x-8)<0) then (-(x-8)) else (x-8)

-- Retorna l'enter que pertany a la columna que està en Char
obtCol :: Char -> Int
obtCol 'a' = 1
obtCol 'b' = 2
obtCol 'c' = 3
obtCol 'd' = 4
obtCol 'e' = 5
obtCol 'f' = 6
obtCol 'g' = 7
obtCol 'h' = 8
obtCol _ = error "Columna no valida"

-- Retorna el Char que pertany a la columna que està en Int
obtColCh :: Maybe Int -> Char
obtColCh (Just 0) = 'a'
obtColCh (Just 1) = 'b'
obtColCh (Just 2) = 'c'
obtColCh (Just 3) = 'd'
obtColCh (Just 4) = 'e'
obtColCh (Just 5) = 'f'
obtColCh (Just 6) = 'g'
obtColCh (Just 7) = 'h'
obtColCh _ = error "Columna no valida"

obtFila :: Maybe Int -> Int
obtFila (Just 0) = 1
obtFila (Just 1) = 2
obtFila (Just 2) = 3
obtFila (Just 3) = 4
obtFila (Just 4) = 5
obtFila (Just 5) = 6
obtFila (Just 6) = 7
obtFila (Just 7) = 8
obtFila _ = 9

{- COSES A MIRAR:
 - regles tauler, que no se salga
 - peon, movimento depende del color
-}

------------- NO UTILITZATS

-- Aquesta funció divideix una array de 64 elements en una array de array's de 8x8
dividirTauler :: [Peca] -> [[Peca]]
dividirTauler x = if (length x ==8) then [x] else ([(take 8 x)] ++ dividirTauler (drop 8 x))
